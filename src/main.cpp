/*
Numbered button matrix:

	1	|	2	|	3
--------+-------+-------
	4	|	5	|	6
--------+-------+-------
	7	|	8	|	9
--------+-------+-------
	10	|	11	|	12

So when you see in serial `Button # 1`
that means a top left button was just pressed
and so on. 
*/


#include <mbed.h>

#define ROW_NUM 4
#define COL_NUM 3

#define ROW_MASK 0xF

static BufferedSerial serial_port(PA_9, PA_10);

FileHandle *mbed::mbed_override_console (int fd) { return &serial_port; }

BusInOut cols(PB_13, PB_14, PB_15);
BusInOut rows(PA_0, PA_1, PA_2, PA_3);

BusOut leds(PB_0, PA_7, PA_6, PA_5);

void setup();
uint8_t scan_matrix();

int main() {
    setup();

    while (1) {
		auto button_num = scan_matrix();
		leds.write(button_num);

		if (button_num > 0) {
        	printf("Button # %u\n", button_num);
		}

		ThisThread::sleep_for(100ms);
    }
}

void setup() {
    serial_port.set_baud(9600);
    serial_port.set_format(8, BufferedSerial::None, 1);

	rows.mode(PullUp);
	cols.mode(PullDown);
}

uint8_t scan_matrix() {
	auto row_bit = 0x8;

	for (int row_idx = 0; row_idx < ROW_NUM; row_idx++) {
		rows.input();
		cols.output();

		// so bits are set to `1` when they are active
		auto row_val = rows.read() ^ ROW_MASK;

		if (row_val & row_bit) {

			rows.output();
			cols.input();

			auto col_bit = 1;

			for (int col_idx = 0; col_idx < COL_NUM; col_idx++) {
				if (cols.read() & col_bit) {
					return (row_idx * COL_NUM) + col_idx + 1;
				}
				col_bit <<= 1;

				ThisThread::sleep_for(20ms);
			}
		}
		row_bit >>= 1;
	}

	return 0;
}
